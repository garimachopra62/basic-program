import java.util.Scanner;



public class Recursion {
	
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		
		System.out.println("Please Enter number ");
		int num = scan.nextInt();
		int factorial = recursive(num);
//		int factorial =1 ;
//		  while (num > 0) {
//			  factorial = factorial * num;
//			  num--;
//		  }
//		//int factorial = recursive(num);
		System.out.println("Factorial "+factorial);
	}

	
	private static int recursive(int num) {
		if(num == 0)
			return 1;

		return num * recursive(num-1);
	}
}

/* Second Method 
 * factorial =1 
 * while (num > 0) {
 *  factorial = factorial * num;
 *  num--;
 *  }
 */
